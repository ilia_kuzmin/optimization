#include "main.h"
#include "mainplugin.h"

#include <QtPlugin>

mainPlugin::mainPlugin(QObject *parent)
    : QObject(parent)
{
    m_initialized = false;
}

void mainPlugin::initialize(QDesignerFormEditorInterface * /* core */)
{
    if (m_initialized)
        return;

    // Add extension registrations, etc. here

    m_initialized = true;
}

bool mainPlugin::isInitialized() const
{
    return m_initialized;
}

QWidget *mainPlugin::createWidget(QWidget *parent)
{
    return new main(parent);
}

QString mainPlugin::name() const
{
    return QLatin1String("main");
}

QString mainPlugin::group() const
{
    return QLatin1String("");
}

QIcon mainPlugin::icon() const
{
    return QIcon();
}

QString mainPlugin::toolTip() const
{
    return QLatin1String("");
}

QString mainPlugin::whatsThis() const
{
    return QLatin1String("");
}

bool mainPlugin::isContainer() const
{
    return false;
}

QString mainPlugin::domXml() const
{
    return QLatin1String("<widget class=\"main\" name=\"main\">\n</widget>\n");
}

QString mainPlugin::includeFile() const
{
    return QLatin1String("main.h");
}
#if QT_VERSION < 0x050000
Q_EXPORT_PLUGIN2(mainplugin, mainPlugin)
#endif // QT_VERSION < 0x050000
