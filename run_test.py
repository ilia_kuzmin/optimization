from simplex_method.tests.test_brute_force import run_brute_force_tests
from simplex_method.tests.test_simplex import run_simplex_tests
from simplex_method.tests.test_dual_problem import run_dual_problem_tests


def run_tests():
    run_brute_force_tests()
    run_simplex_tests()
    run_dual_problem_tests()


run_tests()
