import numpy as np
# from scipy.optimize import linprog as sci_py_simplex
from utils.utils import ContsraintSign, VarSign
from utils.canonical import get_canonical_form
from utils.dual import get_dual
from solvers.simplex import simplex
from solvers.dual_problem import find_dual_solution_from_straight_0_2
from solvers.brute_force import brute_force
from utils.combinations import Combination
from math import fabs
from utils.utils import eps


"""def main_task():
    a_m = np.array([[2., -4., 4., -3., 3., -8., 8., 1., 0.],
                    [7., 1., -1., -3., 3., 12., -12., 0., 1.],
                    [-2., -5., 5., -6., 6., 1., -1., 0., 0.],
                    [-1., -1., 1., 10., -10., 0., 0., 0., 0.]])

    b_v = np.array([3., 5., 6., 1.])
    c_v = np.array([-1., -5., 5., 1., -1., 10., -10., 0., 0.])

    x_opt, _, res, opt_iterations = simplex(a_m, b_v, c_v)
    true_res = sci_py_simplex(method='simplex', c=c_v, A_eq=a_m, b_eq=b_v)

    print(true_res.success)
    print(true_res.fun)
    print(np.dot(x_opt, c_v))
    print(opt_iterations)


def _main_task():
    a_m = np.array([[-2., 4., 3., 8.],
                    [7., 1., -3, 12.],
                    [-2., -5., -6., 1.],
                    [-1., -1., 10., 0.]])
    b_v = np.array([-3., 5., 6., 1.])
    c_v = np.array([-1., -5., 1., 10.])
    consts_signs = [ContsraintSign.GREATER_EQ, ContsraintSign.lESS_EQ, ContsraintSign.EQ, ContsraintSign.EQ]
    vars_signs = [VarSign.GREATER, VarSign.NO_SIGN, VarSign.NO_SIGN, VarSign.NO_SIGN]

    a_can_m, b_can_v, c_can_v = get_canonical_form(a_m, b_v, c_v, consts_signs, vars_signs)

    a_dual_m, b_dual_v, c_dual_v, dual_consts_signs, dual_vars_signs = get_dual(a_m, b_v, c_v, consts_signs, vars_signs)
    a_dc_m, b_dc_v, c_dc_v = get_canonical_form(a_dual_m, b_dual_v, c_dual_v, dual_consts_signs, dual_vars_signs)

    print(a_dual_m)
    print(b_dual_v)
    print(c_dual_v)

    x_opt, res, _ = simplex(a_can_m, b_can_v, c_can_v)
    y_opt, res, _ = simplex(a_dc_m, b_dc_v, -1 * c_dc_v)
    true_res = sci_py_simplex(method='simplex', c=c_dc_v, A_eq=a_dc_m, b_eq=b_dc_v)

    my_y_opt = find_dual_solution_from_straight_0_2(a_m, b_v, c_v, np.array([x_opt[0], x_opt[1] - x_opt[2],
                                                                             x_opt[3] - x_opt[4], x_opt[5] - x_opt[6]]))

    print("my dual solution {}".format(my_y_opt))
    print("ilya dual solution {}".format(y_opt))
    print("my cost value {}".format(np.dot(my_y_opt, b_v)))
    print("ilya cost value {}".format(np.dot(y_opt, c_dc_v)))

    print(true_res.success)
    print(true_res.fun)
    print(np.dot(y_opt, c_dc_v))


_main_task()"""


def task():
    np.set_printoptions(precision=15)

    # initialization

    straight_constraints_matrix = np.array([[-2., 4., 3., 8.],
                                        [7., 1., -3, 12.],
                                        [-2., -5., -6., 1.],
                                        [-1., -1., 10., 0.]])
    straight_constraints_vector = np.array([-3., 5., 6., 1.])
    straight_cost_vector = np.array([-1., -5., 1., 10.])
    straight_constraints_signs = \
        [ContsraintSign.GREATER_EQ, ContsraintSign.lESS_EQ, ContsraintSign.EQ, ContsraintSign.EQ]
    straight_variables_signs = [VarSign.GREATER, VarSign.NO_SIGN, VarSign.NO_SIGN, VarSign.NO_SIGN]

    # getting canonical form of straight problem

    straight_canonical_constraints_matrix, straight_canonical_constraints_vector, straight_canonical_cost_vector = \
        get_canonical_form(straight_constraints_matrix, straight_constraints_vector,
                           straight_cost_vector, straight_constraints_signs, straight_variables_signs)

    # solve straight problem in canonical form

    straight_solution, status, _ = simplex(straight_canonical_constraints_matrix, straight_canonical_constraints_vector,
                                           straight_canonical_cost_vector)

    if status == 0:

        # getting solution of straight problem in general form by using simplex method

        straight_solution_in_general_form = np.array([straight_solution[0],
                                                      straight_solution[1] - straight_solution[2],
                                                      straight_solution[3] - straight_solution[4],
                                                      straight_solution[5] - straight_solution[6]])
        print("solution of straight problem by simplex method {}".format(straight_solution_in_general_form))
        print("value of straight target function {}".format(np.dot(straight_solution_in_general_form, straight_cost_vector)))

        # getting solution of dual problem from solution of straight

        dual_solution_in_general_form = find_dual_solution_from_straight_0_2(straight_constraints_matrix,
                                                                             straight_constraints_vector,
                                                                             straight_cost_vector,
                                                                             straight_solution_in_general_form)

        print("solution of dual problem from solution of straight {}".format(dual_solution_in_general_form))
        print("value of dual target function {}".format(np.dot(dual_solution_in_general_form,
                                                               straight_constraints_vector)))

        # checking of optimality criteria

    # getting dual problem in general form from straight

    dual_constraints_matrix, dual_constraint_vector, dual_cost_vector, dual_constraints_signs, dual_variables_signs = \
        get_dual(straight_constraints_matrix, straight_constraints_vector,
                 straight_cost_vector, straight_constraints_signs, straight_variables_signs)

    # getting canonical form of dual problem

    dual_canonical_constraints_matrix, dual_canonical_constraints_vector, dual_canonical_cost_vector = \
        get_canonical_form(dual_constraints_matrix, dual_constraint_vector,
                           dual_cost_vector, dual_constraints_signs, dual_variables_signs)

    # getting solution of dual problem by simplex method

    dual_solution, status, _ = simplex(dual_canonical_constraints_matrix, dual_canonical_constraints_vector,
                                       -1 * dual_canonical_cost_vector)

    if status == 0:

        # getting solution of dual problem in general form by using simplex method

        dual_solution_in_general_form = np.array([dual_solution[0],
                                                  dual_solution[1],
                                                  dual_solution[2] - dual_solution[3],
                                                  dual_solution[4] - dual_solution[5]])
        print("solution of dual problem by simplex method {}".format(dual_solution_in_general_form))
        print("value of straight target function {}".format(
            np.dot(dual_solution_in_general_form, dual_cost_vector)))

    straight_brute_force_solution, status = brute_force(straight_canonical_constraints_matrix,
                                                        straight_canonical_constraints_vector,
                                                        straight_canonical_cost_vector)

    if status == 0:

        print("solution of straight problem by brute force {}".
              format(np.array([straight_brute_force_solution[0],
                              straight_brute_force_solution[1] - straight_brute_force_solution[2],
                              straight_brute_force_solution[3] - straight_brute_force_solution[4],
                              straight_brute_force_solution[5] - straight_brute_force_solution[6]])))
        print("value of straight target function {}".format(np.dot(straight_brute_force_solution,
                                                                   straight_canonical_cost_vector)))

    dual_brute_force_solution, status = brute_force(dual_canonical_constraints_matrix,
                                                    dual_canonical_constraints_vector,
                                                    -1 * dual_canonical_cost_vector)

    if status == 0:

        print("solution of dual problem by brute force {}".format(np.array([dual_brute_force_solution[0],
                                                                            dual_brute_force_solution[1],
                                                                            dual_brute_force_solution[2] -
                                                                            dual_brute_force_solution[3],
                                                                            dual_brute_force_solution[4] -
                                                                            dual_brute_force_solution[5]])))
        print("value of straight target function {}".format(np.dot(np.array([dual_brute_force_solution[0],
                                                                             dual_brute_force_solution[1],
                                                                             dual_brute_force_solution[2] -
                                                                             dual_brute_force_solution[3],
                                                                             dual_brute_force_solution[4] -
                                                                             dual_brute_force_solution[5]]),
                                                                   dual_cost_vector)))

    rows, columns = straight_canonical_constraints_matrix.shape
    combinations = Combination(rows, columns)
    max_cond = 0
    iterator = iter(combinations)

    for indices in iterator:
        determinant = np.linalg.det(straight_canonical_constraints_matrix[:, indices])

        if fabs(determinant) > eps:
            temporary = np.linalg.cond(straight_canonical_constraints_matrix[:, indices], p=np.inf)
            max_cond = temporary if fabs(temporary - max_cond) > eps else max_cond

    print("maximal conditional number of submatrix - {}".format(max_cond))


task()
