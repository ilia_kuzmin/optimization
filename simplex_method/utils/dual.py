import numpy as np
from .utils import ContsraintSign, VarSign


def get_dual(a_m, b_v, c_v, constraints_signs, vars_signs):
    m, n = a_m.shape

    a_m, b_v, c_v = np.copy(a_m), np.copy(b_v), np.copy(c_v)
    dual_consts_signs, dual_vars_signs = [], []
    for i in range(m):
        if constraints_signs[i] == ContsraintSign.lESS_EQ:
            a_m[i], b_v[i] = -1 * a_m[i], -1 * b_v[i]
            dual_vars_signs.append(VarSign.GREATER)
        elif constraints_signs[i] == ContsraintSign.GREATER_EQ:
            dual_vars_signs.append(VarSign.GREATER)
        elif constraints_signs[i] == ContsraintSign.EQ:
            dual_vars_signs.append(VarSign.NO_SIGN)

    for i in range(n):
        if vars_signs[i] == VarSign.GREATER:
            dual_consts_signs.append(ContsraintSign.lESS_EQ)
        if vars_signs[i] == VarSign.NO_SIGN:
            dual_consts_signs.append(ContsraintSign.EQ)

    return np.transpose(a_m), c_v, b_v, dual_consts_signs, dual_vars_signs
