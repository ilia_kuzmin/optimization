import numpy as np


class Combination:
    def __init__(self, combination_size, source_size):
        self.combination_size = combination_size
        self.source_size = source_size

    def __iter__(self):
        self.current_combination = np.array([x for x in range(self.combination_size)])
        self.current_combination[self.combination_size - 1] -= 1

        return self

    def __next__(self):
        for i in reversed(range(self.combination_size)):

            if self.current_combination[i] < self.source_size - self.combination_size + i:
                self.current_combination[i] += 1

                for j in range(i + 1, self.combination_size):
                    self.current_combination[j] = 1 + self.current_combination[j - 1]

                return self.current_combination

        raise StopIteration
