import numpy as np
from utils.utils import ContsraintSign, VarSign
from utils.utils import eps


def get_unit_matrix_col(m, ind):
    col = np.zeros(m)
    col[ind] = 1

    return col


def get_canonical_form(a_m, b_v, c_v, constraints_signs, var_signs):
    m, n = a_m.shape
    assert m == len(constraints_signs)
    assert n == len(var_signs)

    b_new_v = np.copy(b_v)
    # get rid of inequalities
    extra_v = np.array([])
    for i in range(m):
        if constraints_signs[i] == ContsraintSign.GREATER_EQ:
            col = -1 * get_unit_matrix_col(m, i)
            extra_v = np.hstack((extra_v, col))
        elif constraints_signs[i] == ContsraintSign.lESS_EQ:
            col = get_unit_matrix_col(m, i)
            extra_v = np.hstack((extra_v, col))
    extra_m = np.reshape(extra_v, (m, len(extra_v) // m), order='F')

    # get rid of vars that are not non-negative
    insert_indices, insert_v, insert_cost = [], np.array([]), []
    for i in range(n):
        if var_signs[i] == VarSign.NO_SIGN:
            insert_indices.append(i + 1)
            col = -1 * a_m[:, i]
            insert_v = np.hstack((insert_v, col))
            insert_cost.append(-1 * c_v[i])
        if var_signs[i] == VarSign.LESS:
            a_m[:, i] = -1 * a_m[:, i]
            c_v[i] = -1 * c_v[i]

    insert_m = np.reshape(insert_v, (m, len(insert_v) // m), order='F')

    a_new_m = np.insert(np.hstack((a_m, extra_m)), insert_indices, insert_m, axis=1)
    c_new_v = np.insert(np.hstack((c_v, np.zeros(len(extra_v) // m))), insert_indices, insert_cost)

    # make constant vector non-negative
    for i in range(m):
        if b_new_v[i] < -eps:
            a_new_m[i, :] = -1 * a_new_m[i, :]
            b_new_v[i] = -1 * b_new_v[i]

    return a_new_m, b_new_v, c_new_v

