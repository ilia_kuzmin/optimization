from enum import Enum

eps = 1e-12


def is_zero(val: float):
    return abs(val) < eps


def is_non_positive(val: float):
    return is_zero(val) or val < 0.


def is_non_negative(val: float):
    return is_zero(val) or val > 0.


class ContsraintSign(Enum):
    lESS_EQ = 1
    GREATER_EQ = 2
    EQ = 3


class VarSign(Enum):
    GREATER = 1
    LESS = 2
    NO_SIGN = 3
