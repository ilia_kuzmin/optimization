import math
from .utils import is_zero


def find_pivot(matrix, row_index):
    current_maximum = row_index
    row_number, column_number = matrix.shape

    for current_row in range(row_index, row_number):
        current_maximum = current_row if math.fabs(matrix[current_maximum][row_index]) < \
                                         math.fabs(matrix[current_row][row_index]) else current_maximum

    return current_maximum


def gauss_jordan_method(matrix, vector):
    rows, columns = matrix.shape
    determinant = 1

    for row_index in range(rows):
        pivot_index = find_pivot(matrix, row_index)
        matrix[[row_index, pivot_index]] = matrix[[pivot_index, row_index]]
        vector[[row_index, pivot_index]] = vector[[pivot_index, row_index]]

        if is_zero(matrix[row_index][row_index]):
            return [0, None]

        determinant *= matrix[row_index][row_index]
        vector[row_index] /= matrix[row_index][row_index]
        matrix[row_index] /= matrix[row_index][row_index]

        for i in range(rows):

            if i is not row_index:
                vector[i] -= matrix[i][row_index] * vector[row_index]
                matrix[i] -= matrix[i][row_index] * matrix[row_index]

    return determinant, vector
