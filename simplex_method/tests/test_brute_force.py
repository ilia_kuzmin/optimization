import numpy as np
from scipy.optimize import linprog as sci_py_simplex
from ..solvers.brute_force import brute_force
from ..utils.utils import is_zero, is_non_negative


def test_one():
    c_v = np.array([3., -1., -4., 0., 0.])
    a_m = np.array([[0., -1., 1., 1., 0.],
                    [-5., 1., 1., 0., 0.],
                    [-8., 1., 2., 0., -1.]])
    b_v = np.array([1., 2., 3.])

    x_opt, _ = brute_force(a_m, b_v, c_v)
    delta_opt = np.matmul(a_m, x_opt) - b_v
    assert all(is_zero(elem) for elem in delta_opt), "Brute force: optimal vector does not fit constraints - test 1"
    assert all(is_non_negative(elem) for elem in x_opt), "Brute force: optimal vector is not non-negative - test 1"

    true_res = sci_py_simplex(method='simplex', c=c_v, A_eq=a_m, b_eq=b_v)
    assert is_zero(np.dot(x_opt, c_v) - true_res.fun), "Brute force: wrong answer - test 1"


def test_two():
    a_m = np.array([[2., -2., -10., 10., 3., 0., 0., 0., 0.],
                    [5., -5., -6.5, 6.5, -2.5, 4., -4., 0., 0.],
                    [6., -6., -3.6, 3.6, 0., 7., -7., 1., 0.],
                    [-2., 2., -9., 9., -4., 5., -5., 0., -1.]])
    b_v = np.array([16., 3., 4., 1.])
    c_v = np.array([2., -2., -3., 3., 3., -7., 7., 0., 0.])

    x_opt, _ = brute_force(a_m, b_v, c_v)

    delta_opt = np.matmul(a_m, x_opt) - b_v
    assert all(is_zero(elem) for elem in delta_opt), "Brute force: optimal vector does not fit constraints - test 2"
    assert all(is_non_negative(elem) for elem in x_opt), "Brute force: optimal vector is not non-negative - test 2"

    true_res = sci_py_simplex(method='simplex', c=c_v, A_eq=a_m, b_eq=b_v)
    assert is_zero(np.dot(x_opt, c_v) - true_res.fun), "Brute force: wrong answer - test 2"


def test_three():
    a_m = np.array([[1., 1., 1., 0., 0.],
                    [2., 3., 0., 1., 0.],
                    [12., 30., 0., 0., 1.]])
    b_v = np.array([55., 120., 960.])
    c_v = np.array([-3., -4., 0., 0., 0.])

    x_opt, _ = brute_force(a_m, b_v, c_v)

    delta_opt = np.matmul(a_m, x_opt) - b_v
    assert all(is_zero(elem) for elem in delta_opt), "Brute force: optimal vector does not fit constraints - test 3"
    assert all(is_non_negative(elem) for elem in x_opt), "Brute force: optimal vector is not non-negative - test 3"

    true_res = sci_py_simplex(method='simplex', c=c_v, A_eq=a_m, b_eq=b_v)
    assert is_zero(np.dot(x_opt, c_v) - true_res.fun), "Brute force: wrong answer - test 3"


def test_four():
    a_m = np.array([[1., 0., 0., 1., 1., 1., 1.],
                    [-2., 1., 0., 1., -3., 4., 0.],
                    [3., 0., 1., 4., -2., 1., 0.]])
    b_v = np.array([1., 0., 0.])
    c_v = np.array([1., 0., 0., -1., -1., 1., 0.])

    x_opt, _ = brute_force(a_m, b_v, c_v)

    delta_opt = np.matmul(a_m, x_opt) - b_v
    assert all(is_zero(elem) for elem in delta_opt), "Brute force: optimal vector does not fit constraints - test 4"
    assert all(is_non_negative(elem) for elem in x_opt), "Brute force: optimal vector is not non-negative - test 4"

    true_res = sci_py_simplex(method='simplex', c=c_v, A_eq=a_m, b_eq=b_v)
    assert is_zero(np.dot(x_opt, c_v) - true_res.fun), "Brute force: wrong answer - test 4"


def test_five():
    a_m = np.array([[0.25, -60., -0.04, 9., 1., 0., 0.],
                    [0.5, -90., -0.02, 3., 0., 1., 0.],
                    [0., 0., 1., 0., 0., 0., 1.]])
    b_v = np.array([0., 0., 1.])
    c_v = np.array([-0.75, 150., 0.02, 6., 0., 0., 0.])

    x_opt, _ = brute_force(a_m, b_v, c_v)

    delta_opt = np.matmul(a_m, x_opt) - b_v
    assert all(is_zero(elem) for elem in delta_opt), "Brute force: optimal vector does not fit constraints - test 5"
    assert all(is_non_negative(elem) for elem in x_opt), "Brute force: optimal vector is not non-negative - test 5"

    true_res = sci_py_simplex(method='simplex', c=c_v, A_eq=a_m, b_eq=b_v)
    assert is_zero(np.dot(x_opt, c_v) - true_res.fun), "Brute force: wrong answer - test 5"


def test_six():
    a_m = np.array([[0., -1., 0., 0., 1., 1., 0.],
                    [2., 2., 1., 0., 2., 0., 3.],
                    [1., 1., 0., 1., 1., 0., 2.]])
    b_v = np.array([1., 0., 0.])
    c_v = np.array([3., 2., 1., 0., 0., 1., 0.])

    x_opt, _ = brute_force(a_m, b_v, c_v)

    delta_opt = np.matmul(a_m, x_opt) - b_v
    assert all(is_zero(elem) for elem in delta_opt), "Brute force: optimal vector does not fit equalities - test 6"
    assert all(is_non_negative(elem) for elem in x_opt), "Brute force: optimal vector is not non-negative - test 6"

    true_res = sci_py_simplex(method='simplex', c=c_v, A_eq=a_m, b_eq=b_v)
    assert is_zero(np.dot(x_opt, c_v) - true_res.fun), "Brute force: wrong answer - test 6"


def run_brute_force_tests():
    test_one()
    test_two()
    test_three()
    test_four()
    test_five()
    test_six()


run_brute_force_tests()
