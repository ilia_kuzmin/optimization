import numpy as np
from scipy.optimize import linprog as sci_py_simplex
from solvers.dual_problem import find_dual_solution_from_straight_0_1, find_dual_solution_from_straight_0_2
from utils.utils import is_zero


def test_one():
    c_v = np.array([3., -1., -4., 0., 0.])
    a_m = np.array([[0., -1., 1., 1., 0.],
                    [-5., 1., 1., 0., 0.],
                    [-8., 1., 2., 0., -1.]])
    b_v = np.array([1., 2., 3.])

    str_sol = sci_py_simplex(method='simplex', c=c_v, A_eq=a_m, b_eq=b_v)

    y_opt = find_dual_solution_from_straight_0_1(a_m, b_v, c_v, str_sol.x)
    assert is_zero(np.dot(y_opt, b_v) - str_sol.fun), "Dual problem: wrong answer in version 0.1 - test 1"
    y_opt = find_dual_solution_from_straight_0_2(a_m, b_v, c_v, str_sol.x)
    assert is_zero(np.dot(y_opt, b_v) - str_sol.fun), "Dual problem: wrong answer in version 0.2 - test 1"


def test_two():
    a_m = np.array([[2., -2., -10., 10., 3., 0., 0., 0., 0.],
                    [5., -5., -6.5, 6.5, -2.5, 4., -4., 0., 0.],
                    [6., -6., -3.6, 3.6, 0., 7., -7., 1., 0.],
                    [-2., 2., -9., 9., -4., 5., -5., 0., -1.]])
    b_v = np.array([16., 3., 4., 1.])
    c_v = np.array([2., -2., -3., 3., 3., -7., 7., 0., 0.])

    str_sol = sci_py_simplex(method='simplex', c=c_v, A_eq=a_m, b_eq=b_v)

    y_opt = find_dual_solution_from_straight_0_1(a_m, b_v, c_v, str_sol.x)
    assert is_zero(np.dot(y_opt, b_v) - str_sol.fun), "Dual problem: wrong answer in version 0.1 - test 2"
    y_opt = find_dual_solution_from_straight_0_2(a_m, b_v, c_v, str_sol.x)
    assert is_zero(np.dot(y_opt, b_v) - str_sol.fun), "Dual problem: wrong answer in version 0.2 - test 2"


def test_three():
    a_m = np.array([[1., 1., 1., 0., 0.],
                    [2., 3., 0., 1., 0.],
                    [12., 30., 0., 0., 1.]])
    b_v = np.array([55., 120., 960.])
    c_v = np.array([-3., -4., 0., 0., 0.])

    str_sol = sci_py_simplex(method='simplex', c=c_v, A_eq=a_m, b_eq=b_v)

    y_opt = find_dual_solution_from_straight_0_1(a_m, b_v, c_v, str_sol.x)
    assert is_zero(np.dot(y_opt, b_v) - str_sol.fun), "Dual problem: wrong answer in version 0.1 - test 3"
    y_opt = find_dual_solution_from_straight_0_2(a_m, b_v, c_v, str_sol.x)
    assert is_zero(np.dot(y_opt, b_v) - str_sol.fun), "Dual problem: wrong answer in version 0.2 - test 3"


def test_four():
    a_m = np.array([[1., 0., 0., 1., 1., 1., 1.],
                    [-2., 1., 0., 1., -3., 4., 0.],
                    [3., 0., 1., 4., -2., 1., 0.]])
    b_v = np.array([1., 0., 0.])
    c_v = np.array([1., 0., 0., -1., -1., 1., 0.])

    str_sol = sci_py_simplex(method='simplex', c=c_v, A_eq=a_m, b_eq=b_v)

    y_opt = find_dual_solution_from_straight_0_1(a_m, b_v, c_v, str_sol.x)
    assert is_zero(np.dot(y_opt, b_v) - str_sol.fun), "Dual problem: wrong answer in version 0.1 - test 4"
    y_opt = find_dual_solution_from_straight_0_2(a_m, b_v, c_v, str_sol.x)
    assert is_zero(np.dot(y_opt, b_v) - str_sol.fun), "Dual problem: wrong answer in version 0.2 - test 4"


def test_five():
    a_m = np.array([[0.25, -60., -0.04, 9., 1., 0., 0.],
                    [0.5, -90., -0.02, 3., 0., 1., 0.],
                    [0., 0., 1., 0., 0., 0., 1.]])
    b_v = np.array([0., 0., 1.])
    c_v = np.array([-0.75, 150., 0.02, 6., 0., 0., 0.])

    str_sol = sci_py_simplex(method='simplex', c=c_v, A_eq=a_m, b_eq=b_v)

    y_opt = find_dual_solution_from_straight_0_1(a_m, b_v, c_v, str_sol.x)
    assert is_zero(np.dot(y_opt, b_v) - str_sol.fun), "Dual problem: wrong answer in version 0.1 - test 5"
    y_opt = find_dual_solution_from_straight_0_2(a_m, b_v, c_v, str_sol.x)
    assert is_zero(np.dot(y_opt, b_v) - str_sol.fun), "Dual problem: wrong answer in version 0.2 - test 5"


def test_six():
    a_m = np.array([[0., -1., 0., 0., 1., 1., 0.],
                    [2., 2., 1., 0., 2., 0., 3.],
                    [1., 1., 0., 1., 1., 0., 2.]])
    b_v = np.array([1., 0., 0.])
    c_v = np.array([3., 2., 1., 0., 0., 1., 0.])

    str_sol = sci_py_simplex(method='simplex', c=c_v, A_eq=a_m, b_eq=b_v)

    y_opt = find_dual_solution_from_straight_0_1(a_m, b_v, c_v, str_sol.x)
    assert is_zero(np.dot(y_opt, b_v) - str_sol.fun), "Dual problem: wrong answer in version 0.1 - test 6"
    y_opt = find_dual_solution_from_straight_0_2(a_m, b_v, c_v, str_sol.x)
    assert is_zero(np.dot(y_opt, b_v) - str_sol.fun), "Dual problem: wrong answer in version 0.2 - test 6"


def run_dual_problem_tests():
    test_one()
    test_two()
    test_three()
    test_four()
    test_five()
    test_six()
