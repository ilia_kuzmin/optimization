import numpy as np
import math
from utils.gauss_jordan import gauss_jordan_method
from utils.combinations import Combination
from utils.utils import is_zero, is_non_negative


def is_valid(solution):
    return all(is_non_negative(elem) for elem in solution)


#   @:param matrix - constraint matrix
#   @:param vector - constraint vector(column)
#   @:param cost_vector - coefficients of cost function(row)

#   @:return - optimal solution, result
#     result = 0 if solution has been successfully found
#            = 1 if no solution has been found
def brute_force(constraint_matrix, constraint_vector, cost_vector):
    rows, columns = constraint_matrix.shape

    combination = Combination(rows, columns)
    iterator = iter(combination)
    opt_val, opt_vec, opt_ind = math.inf, None, None

    for indices in iterator:
        current_matrix = constraint_matrix[:, indices]
        determinant, tmp = gauss_jordan_method(np.copy(current_matrix), np.copy(constraint_vector))

        if not is_zero(determinant) and is_valid(tmp):
            current_cost = cost_vector[indices]

            if np.dot(current_cost, tmp) < opt_val:
                opt_val, opt_vec, opt_ind = np.dot(current_cost, tmp), np.copy(tmp), np.copy(indices)

    if opt_val is math.inf:
        return np.array([]), 1

    fixed_opt_vec = np.zeros(columns)
    fixed_opt_vec[opt_ind] = opt_vec
    return fixed_opt_vec, 0
