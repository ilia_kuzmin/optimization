import numpy as np
from utils.combinations import Combination
from utils.gauss_jordan import gauss_jordan_method
from utils.dual import get_dual
from utils.utils import ContsraintSign, VarSign, is_zero


def non_zero_elements(straight_solution):
    sum = 0
    indices = []
    size, = straight_solution.shape

    for i in range(size):
        if not is_zero(straight_solution[i]):
            sum += 1
            indices.append(i)

    return sum, indices


def get_not_used_columns(columns, used_indices):
    not_used_indices = []

    for index in range(columns):
        if index not in used_indices:
            not_used_indices.append(index)

    return not_used_indices


def find_dual_solution_from_straight_0_1(straight_constraint_matrix, straight_constraint_vector,
                                     straight_cost_vector, straight_solution_vector):
    dual_constraint_matrix, dual_constraint_vector, dual_cost_vector, _, _ = \
        get_dual(straight_constraint_matrix,
                 straight_constraint_vector,
                 straight_cost_vector,
                 [ContsraintSign.EQ for _ in range(straight_constraint_matrix.shape[0])],
                 [VarSign.GREATER for _ in range(straight_constraint_matrix.shape[1])])
    sum, used_indices = non_zero_elements(straight_solution_vector)

    if sum == dual_cost_vector.shape[0]:
        current_matrix = np.transpose(np.copy(straight_constraint_matrix[:, used_indices]))
        current_vector = np.copy(dual_constraint_vector[used_indices])
        determinant, solution = gauss_jordan_method(current_matrix, current_vector)

        return solution

    else:
        rows, columns = straight_constraint_matrix.shape
        not_used_indices = get_not_used_columns(columns, used_indices)

        combination = Combination(dual_cost_vector.shape[0] - sum, len(not_used_indices))
        iterator = iter(combination)
        for indices in iterator:
            current_indices = used_indices + [not_used_indices[ind] for ind in indices]
            current_matrix = np.transpose(np.copy(straight_constraint_matrix[:, current_indices]))
            current_vector = np.copy(dual_constraint_vector[current_indices])
            determinant, solution = gauss_jordan_method(current_matrix, current_vector)

            if not is_zero(determinant):
                return solution


def find_dual_solution_from_straight_0_2(straight_constraint_matrix, straight_constraint_vector,
                                     straight_cost_vector, straight_solution_vector):
    dual_solution_vector = np.zeros(straight_constraint_vector.shape[0])
    not_zero_indices_straight = []
    not_zero_indices_dual = []

    for i in range(straight_constraint_vector.shape[0]):

        if is_zero(straight_constraint_matrix[i].dot(straight_solution_vector) - straight_constraint_vector[i]):
            not_zero_indices_dual.append(i)
            
    for i in range(straight_solution_vector.shape[0]):
        
        if not is_zero(straight_solution_vector[i]):
            not_zero_indices_straight.append(i)

    determinant, solution = \
        gauss_jordan_method(
            np.transpose((straight_constraint_matrix[not_zero_indices_dual])[: , not_zero_indices_straight]),
                            np.transpose(straight_cost_vector[not_zero_indices_straight]))

    dual_solution_vector[not_zero_indices_dual] = solution

    return dual_solution_vector
