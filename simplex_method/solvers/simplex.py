import numpy as np
from utils.utils import is_zero, is_non_negative, is_non_positive


def get_zero_indices(x_v: np.ndarray, n: int, n_cur: list):
    assert len(x_v.shape) == 1

    return [index for index in range(n) if n_cur[index] and is_zero(x_v[index])]


def get_negative_component_index(d_v: np.ndarray, l_cur: list):
    assert len(d_v.shape) == 1

    for elem, index in zip(d_v, l_cur):
        if not is_non_negative(elem):
            return index
    return -1


def get_fixed_excluded_index(excluded_index: int, n_cur: list, n: int):
    fixed_excluded_index = 0
    for i in range(n):
        if n_cur[i]:
            if i == excluded_index:
                break
            fixed_excluded_index += 1
    return fixed_excluded_index


def get_fixed_included_index(included_index: int, excluded_index: int, n_cur: list, n: int):
    fixed_included_index = 0
    for i in range(n):
        if i == included_index:
            break
        if n_cur[i] and not i == excluded_index:
            fixed_included_index += 1
    return fixed_included_index


def is_unbounded(u_v: np.ndarray, n: int, n_cur: list):
    for index in range(n):
        if n_cur[index] and not is_non_positive(u_v[index]):
            return False
    return True


def is_good_degenerate_vector(u_v: np.ndarray, zero_indices: list):
    tmp = u_v[zero_indices]
    for elem in tmp:
        if not is_non_positive(elem):
            return False
    return True


def swap_cols(a_m: np.ndarray, i: int, j: int):
    tmp = np.copy(a_m[i, :])
    a_m[i, :] = a_m[j, :]
    a_m[j, :] = tmp


# b_cur_m - current support-vector's inverse matrix
# u_v[n_cur] - subsidiary vector
# excluded_index - from 0 to len(n_cur)
def get_next_inverse_matrix(b_cur_m: np.ndarray, u_v: np.ndarray, excluded_index: int, included_index: int):
    assert len(b_cur_m.shape) == 2
    assert b_cur_m.shape[0] == b_cur_m.shape[1]

    size = b_cur_m.shape[0]
    f_m = np.identity(size)
    col = [1 / u_v[excluded_index] if i == excluded_index else -u_v[i] / u_v[excluded_index] for i in range(size)]
    f_m[:, excluded_index] = np.array(col)
    inverse_m = np.matmul(f_m, b_cur_m)

    if included_index < excluded_index:
        for i in range(excluded_index, included_index, -1):
            swap_cols(inverse_m, i, i - 1)
    else:
        for i in range(excluded_index, included_index):
            swap_cols(inverse_m, i, i + 1)

    return inverse_m


def get_direction_vector(n: int, a_m: np.ndarray, b_cur_m: np.ndarray, n_cur: list, j_cur: int):
    tmp_v = np.matmul(b_cur_m, a_m[:, j_cur])

    u_v = np.zeros(n)
    tmp_index = 0
    for index in range(n):
        if n_cur[index]:
            u_v[index] = tmp_v[tmp_index]
            tmp_index += 1
    u_v[j_cur] = -1.

    return u_v


def get_next_support_vector(x_cur_v: np.ndarray, u_v: np.ndarray, n_cur: list, n: int):
    theta = float("Inf")
    min_index = -1
    for index in range(n):
        if n_cur[index] and not is_non_positive(u_v[index]):
            val = x_cur_v[index] / u_v[index]
            if val < theta:
                theta = val
                min_index = index

    assert min_index != -1

    return x_cur_v - theta * u_v, min_index


# change basis using Bland's rule to avoid cycling
def change_basis(a_m: np.ndarray, b_m: np.ndarray, n_cur: list, included_ind: int, zero_indices: list, u_v: np.ndarray):
    n = a_m.shape[1]
    excluded_ind = 0
    for zero_ind in zero_indices:
        if not is_non_positive(u_v[zero_ind]):
            excluded_ind = zero_ind
            break
    b_m = get_next_inverse_matrix(b_m, np.matmul(b_m, a_m[:, included_ind]),
                                  get_fixed_excluded_index(excluded_ind, n_cur, n),
                                  get_fixed_included_index(included_ind, excluded_ind, n_cur, n))
    n_cur[excluded_ind], n_cur[included_ind] = False, True

    return b_m


# solving linear programming problem in canonical form
# a_m - matrix of constraints
# b_v - constant vector
# x_init_v - initial support-vector
# b_init_m - reverse matrix of support-vector
# n_init - basis indices of support-vector (n_init[i] == True - if i in the basis)
# c_v - cost vector
# returns optimal solution, inverse matrix of basis columns,
# number of iterations to get optimal solution, result
# result = 0 - optimal solution has been successfully found
#        = 1 - no finite solution, cost function is unbounded
def _simplex(a_m: np.ndarray, x_init_v: np.ndarray, b_init_m, n_init: list, c_v: np.ndarray):
    assert len(a_m.shape) == 2
    # m - number of rows, n - number of columns of constraints matrix
    m, n = a_m.shape
    assert m < n, "Incorrect matrix"

    x_cur_v, b_cur_m, n_cur = x_init_v, b_init_m, n_init

    iterations = 1
    while True:
        l_cur = [i for i in range(n) if not n_cur[i]]
        n_cur_indices = [i for i in range(n) if n_cur[i]]
        d_cur_v = c_v[l_cur] - np.matmul(np.matmul(c_v[n_cur_indices], b_cur_m), a_m[:, l_cur])
        j_cur = get_negative_component_index(d_cur_v, l_cur)

        # if all components of d_k_v are non-negative, then x_cur_v is an optimal solution
        if j_cur == -1:
            break

        u_v = get_direction_vector(n, a_m, b_cur_m, n_cur, j_cur)

        # cost vector has no finite minimum
        if is_unbounded(u_v, n, n_cur):
            x_cur_v = np.array([])
            return np.array([]), np.array([[]]), 1, -1

        zero_indices = get_zero_indices(x_cur_v, n, n_cur)
        # if current support-vector is non-degenerate or degenerate, but it is unnecessary to change basis
        if len(zero_indices) == 0 or is_good_degenerate_vector(u_v, zero_indices):
            x_cur_v, excluded_index = get_next_support_vector(x_cur_v, u_v, n_cur, n)
            b_cur_m = get_next_inverse_matrix(b_cur_m, u_v[n_cur_indices],
                                              get_fixed_excluded_index(excluded_index, n_cur, n),
                                              get_fixed_included_index(j_cur, excluded_index, n_cur, n))
            n_cur[j_cur], n_cur[excluded_index] = True, False

            iterations += 1
            continue

        b_cur_m = change_basis(a_m, b_cur_m, n_cur, j_cur, zero_indices, u_v)
        iterations += 1

    return x_cur_v, b_cur_m, 0, iterations


# if initial vector is degenerate, columns of unit matrix should be excluded from the basis (if they are in the basis)
def fix_initial_basis(d_m: np.ndarray, b_init_m: np.ndarray, n_start: list, n: int, m: int):
    for excluded_ind in range(n, n + m):
        if not n_start[excluded_ind]:
            continue
        for included_ind in range(n):
            if n_start[included_ind]:
                continue
            if is_zero(np.linalg.det(d_m[:, [j if not j == excluded_ind else included_ind
                                             for j in range(n + m) if n_start[j]]])):
                continue
            b_init_m = get_next_inverse_matrix(b_init_m, np.matmul(b_init_m, d_m[:, included_ind]),
                                               get_fixed_excluded_index(excluded_ind, n_start, n + m),
                                               get_fixed_included_index(included_ind, excluded_ind, n_start, n))
            n_start[included_ind], n_start[excluded_ind] = True, False
            break
    return b_init_m


def get_init_basis(n_start: list, n: int):
    n_init = {}
    for i in range(n):
        n_init[i] = n_start[i]
    return n_init


# a_m - matrix of constraints
# b_v - constant vector
# returns initial support-vector, inverse matrix of basis columns, list of basis columns,
# number of iterations to get initial vector, result
# result = 0 - initial vector has been successfully found
#        = 1 - the set of valid vectors is empty
def get_first_support_vector(a_m: np.ndarray, b_v: np.ndarray):
    assert len(a_m.shape) == 2

    m, n = a_m.shape
    n_start = [False if i < n else True for i in range(n + m)]

    d_m = np.hstack((a_m, np.identity(m)))
    c_init = np.hstack((np.zeros(n), np.ones(m)))
    z_init_v, b_init_m, res, iters = _simplex(d_m, np.hstack((np.zeros(n), b_v)),
                                                  np.identity(m), n_start, c_init)
    assert res == 0

    x_init_v, y_init_v = z_init_v[range(n)], z_init_v[range(n, n + m)]
    # if the set is empty
    if len([elem for elem in y_init_v if not is_zero(elem)]) != 0:
        return np.array([]), np.array([]), [], 1, -1

    zero_indices = get_zero_indices(x_init_v, n, n_start)
    # if x_init_v is degenerate vector
    if len(zero_indices) != 0:
        b_init_m = fix_initial_basis(d_m, b_init_m, n_start, n, m)

    n_init = get_init_basis(n_start, n)

    return x_init_v, b_init_m, n_init, 0, iters


# returns optimal solution, result, number of iterations to get optimal solution
# result = 0 - optimal solution has been successfully found
#        = 1 - the set of valid vectors is empty
#        = 2 - no finite solution, cost function is unbounded
def simplex(a_m: np.ndarray, b_v: np.ndarray, c_v: np.ndarray):
    x_init_v, b_init_m, n_init, res_init, iters_init = get_first_support_vector(a_m, b_v)

    if res_init == 1:
        return np.array([]), 1, 0

    x_opt, _, res_opt, iters_opt = _simplex(a_m, x_init_v, b_init_m, n_init, c_v)

    if res_opt == 1:
        return np.array([]), 2, 0

    return x_opt, 0, iters_init + iters_opt
